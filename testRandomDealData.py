import unittest
from randomDealData import *


class TestRandomDealData(unittest.TestCase):
    def testRandomDealDataResult(self):
        rdd = RandomDealData()
        instr_list = rdd.createInstrumentList()
        assert isinstance(instr_list, list), 'InstrumentList should be a list, not a ' + str(type(instr_list))
        assert len(instr_list) == len(instruments),\
            'Len of InstrumentList should be ' + str(len(instruments)) + ' not ' + str(len(instr_list))
        response = rdd.createRandomData(instr_list)
        assert isinstance(response, str), 'Result should be string, not a ' + str(type(response))

    def testTypeOfRandomDealDataResult(self):
        num = 10
        response = generate_mutable_deals(num)
        assert isinstance(response, list), 'Result should be list, not ' + str(type(response))
        assert len(response) == num, 'Len of result`s list should be ' + str(num) + ', not ' + str(len(response))
        for dict_i in response:
            assert isinstance(dict_i, dict), 'Every element of list result should be dict, not ' + str(type(dict_i))
            key_list = ['instrumentName', 'cpty', 'price', 'type', 'quantity', 'time']
            assert set(dict_i.keys()) == set(key_list), 'Wrong set of dict`s keys: ' + str(dict_i.keys())
            assert isinstance(dict_i['instrumentName'], str), \
                'This variable should be str, not ' + str(type(dict_i['instrumentName']))
            assert isinstance(dict_i['cpty'], str), \
                'InstrumentName should be str, not ' + str(type(dict_i['cpty']))
            assert isinstance(dict_i['price'], float), \
                'Price should be float, not ' + str(type(dict_i['price']))
            assert isinstance(dict_i['type'], str) and len(dict_i['type']) == 1, \
                'Type should be str with len = 1, not ' + str(type(dict_i['type'])) +\
                ' len: ' + str(len(dict_i['type']))
            assert isinstance(dict_i['quantity'], int),\
                'Quantity should be int, not ' + str(type(dict_i['quantity']))
            assert isinstance(dict_i['time'], str), \
                'Time should be str, not ' + str(type(dict_i['time']))


if __name__ == '__main__':
    unittest.main()

