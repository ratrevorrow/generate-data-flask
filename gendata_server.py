from flask import Flask, jsonify
from flask_cors import CORS
from randomDealData import generate_mutable_deals

app = Flask(__name__)
CORS(app)

NUM_DEALS = 50

@app.route("/generateData")
def generate_data():
    answer = generate_mutable_deals(NUM_DEALS)
    return jsonify(answer)


def bootapp():
    app.run(port=7000, threaded=True, host=('0.0.0.0'))

if __name__ == '__main__':
     bootapp()